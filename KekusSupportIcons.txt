; This Add-on is not created by, affiliated with or sponsored by ZeniMax Media
; Inc. or its affiliates. The Elder Scrolls and related logos are registered
; trademarks or trademarks of ZeniMax Media Inc. in the United States and/or
; other countries. All rights reserved.
; https://account.elderscrollsonline.com/add-on-terms

## Title: KekusSupportIcons
## Description: Show rofl icons for players in your group
## Author: @KekusTheDivine
## Version: 1.0.1
## APIVersion: 100033
## SavedVariables: KSIStore
## DependsOn: LibAddonMenu-2.0>=31

KekusSupportIcons.lua
anim.lua
Canim.lua
UserIcons.lua
Bindings.xml

