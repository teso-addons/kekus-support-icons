KekusSupportIcons = {}
local KSI = KekusSupportIcons

KSI.name 		= "KekusSupportIcons"
KSI.version 	= "1.0.1"
KSI.author 		= "@KekusTheDivine,@Forsion"
KSI.show        = true  
KSI.animshow	= true  -- show animated icons
KSI.youricon	= true  -- show your icon in group list
KSI.debug       = false
KSI.player      = string.lower( GetUnitDisplayName( "player" ) )
KSI.events		= GetEventManager()
KSI.window 		= GetWindowManager()
KSI.group       = { }
KSI.special     = { }
KSI.animspecial = { }

-- libraries
local LAM = LibAddonMenu2


-- used for custom icons
local LFG_ROLE_SPECIAL = 99
-- used for leader icon
local LFG_ROLE_LEADER = 98
-- used for animated icons
local LFG_ROLE_ANIMATED = 97
-- used for custom animated icons
local LFG_ROLE_CUSTOMANIMATED = 96

-- default color
local BASECOLOR = { 1, 1, 1 }

local DEFAULT = {
	["youricon"]	= false,
    ["animicons"]   = false,
    ["interval"]    = 10,
    ["iconsize"]    = 100,
    ["offset"]      = 3.5,
    ["alpha"]       = 1,
    ["scalefactor"] = 1,
    ["scaling"]     = true,
    ["save"]        = true,
    ["ignore"]      = false,
    ["cache"]       = { },	
	["animatedspecial"]  = { },
    [LFG_ROLE_LEADER]  = {
        ["show"]  = true,
        ["color"] = { 1, 0, 1 },  -- pink
        ["icon"]  = "esoui/art/icons/mapkey/mapkey_groupleader.dds",
    },
    [LFG_ROLE_TANK] = {
        ["show"]  = true,
        ["color"] = { 1, 1, 0 },  -- yellow
        ["icon"]  = "esoui/art/lfg/gamepad/lfg_roleicon_tank.dds",
    },
    [LFG_ROLE_HEAL] = {
        ["show"]  = true,
        ["color"] = { 0, 1, 1 },  -- teal
        ["icon"]  = "esoui/art/lfg/gamepad/lfg_roleicon_healer.dds",
    },
    [LFG_ROLE_DPS]  = {
        ["show"]  = true,
        ["color"] = { 1, 0, 1 },  -- pink
        ["icon"]  = "esoui/art/lfg/gamepad/lfg_roleicon_dps.dds",
    },	
}


-- debug
local ERRORS = {
    [1] = "does not exist",
    [2] = "is you",
    [3] = "is not a player",
    [4] = "is not online",
    [5] = "is not grouped",
    [6] = "is not in same instance",
    [7] = "is not in same world",
    [8] = "is in remote region",
    [9] = "has not the right role",
}


local function tableAppend( tab, add )
	for _, v in ipairs( add ) do
		table.insert( tab, v )
	end
end

function KSI.CreateUI()
    -- create render space control
    KSI.ctrl = KSI.window:CreateControl( "KSICtrl", GuiRoot, CT_CONTROL )
    KSI.ctrl:Create3DRenderSpace()
    -- create parent window for icons
	KSI.win = KSI.window:CreateTopLevelWindow( "KSIWin" )
    KSI.win:SetClampedToScreen( true )
    KSI.win:SetMouseEnabled( false )
    KSI.win:SetMovable( false )
	KSI.win:ClearAnchors()
	KSI.win:SetAnchor( CENTER, GuiRoot, CENTER, 0, 0 )
    KSI.win:SetDimensions( GuiRoot:GetWidth(), GuiRoot:GetHeight() )
	KSI.win:SetDrawLayer( 0 )
	KSI.win:SetDrawLevel( 0 )
	KSI.win:SetDrawTier( 0 )
    -- create parent window scene fragment
	local frag = ZO_HUDFadeSceneFragment:New( KSI.win )
	HUD_UI_SCENE:AddFragment( frag )
    HUD_SCENE:AddFragment( frag )
end

function KSI.CreateUICustom()
    -- create custom icon picker window
	KSI.pick = KSI.window:CreateTopLevelWindow( "KSIPick" )
    KSI.pick:SetClampedToScreen( true )
    KSI.pick:SetMouseEnabled( true )
    KSI.pick:SetMovable( false )
	KSI.pick:ClearAnchors()
	KSI.pick:SetAnchor( CENTER, GuiRoot, CENTER, 0, 0 )
    KSI.pick:SetDimensions( 350, 256 )
    KSI.pick:SetHidden( true )
    -- create window backdrop
    local back = KSI.window:CreateControl( "KSIPickBack", KSI.pick, CT_BACKDROP )
    back:ClearAnchors()
    back:SetAnchor( TOPLEFT, KSI.pick, TOPLEFT, 0, 0 )
    back:SetDimensions( 350, 256 )
    back:SetEdgeTexture( "EsoUI/Art/Tooltips/UI-Border.dds", 128, 16 )
    back:SetCenterTexture( "EsoUI/Art/Tooltips/UI-TooltipCenter.dds" )
    back:SetInsets( 16, 16, -16, -16 )
    back.data = { }
    -- create window title
    local label = KSI.window:CreateControl( "KSIPickLabel", back, CT_LABEL )
    label:ClearAnchors()
    label:SetAnchor( TOPLEFT, back, TOPLEFT, 16, 16 )
    label:SetDimensions( 318, 48 )
    label:SetFont( "ZoFontWinH4" )
    -- create color picker
    local colordata = {
        type     = "colorpicker",
        name     = "Color",
        getFunc  = function() return unpack( KSI.custom.color ) end,
        setFunc  = function( r, g, b, a ) KSI.custom.color = { r, g, b } end,
        disabled = function() return KSI.custom.texture:find( "^kekus" ) ~= nil end,
    }
    local color = LAMCreateControl.colorpicker( back, colordata, "KSIPickColor" )
    color:ClearAnchors()
    color:SetAnchor( TOPLEFT, label, BOTTOMLEFT, 0, 0 )
    -- create icon picker
    local icondata = {
        type       = "iconpicker",
        name       = "Texture",
        iconSize   = 64,
        maxColumns = 4,
        getFunc    = function() return KSI.custom.texture end,
        setFunc    = function( newValue ) KSI.custom.texture = newValue; KSI.pickcolor.UpdateDisabled( KSI.pickcolor ) end,
        choices    = KSI.ICONS,
    }
    local icon = LAMCreateControl.iconpicker( back, icondata, "KSIPickIcon" )
    icon:ClearAnchors()
    icon:SetAnchor( TOPLEFT, color, BOTTOMLEFT, 0, 16 )
    -- create save button 
    local savedata = {
        type  = "button",
        name  = "SAVE",
        func  = function() KSI.SetIconForUnit( KSI.custom.displayName, KSI.custom.texture, KSI.custom.color ); KSI.pick:SetHidden( true ) end,
        width = "half",
    }
    local save = LAMCreateControl.button( back, savedata, "KSIPickSave" )
    save:ClearAnchors()
    save:SetAnchor( TOPLEFT, icon, BOTTOMLEFT, 0, 16 )
    -- create cancel button
    local canceldata = {
        type  = "button",
        name  = "CANCEL",
        func  = function() KSI.pick:SetHidden( true ) end,
        width = "half",
    }
    local cancel = LAMCreateControl.button( back, canceldata, "KSIPickCancel" )
    cancel:ClearAnchors()
    cancel:SetAnchor( TOPRIGHT, icon, BOTTOMRIGHT, 14, 16 )    
    -- store window title and controls
    KSI.picklabel = label
    KSI.pickcolor = color
    KSI.pickicon  = icon
    -- hide window if group menu is closed
    local function sceneCheck( showing )
        if showing ~= "showing" then
            -- TODO: fix bug
            -- LAM.util.GetIconPickerMenu().control:SetHidden( true )
            LAM.util.GetIconPickerMenu():Clear()
            KSI.pick:SetHidden( true )
        end
    end
    KEYBOARD_GROUP_MENU_SCENE:RegisterCallback( "StateChange", sceneCheck )
    GAMEPAD_GROUP_SCENE:RegisterCallback( "StateChange", sceneCheck )
end

function KSI.CreateSupportIcon( unit, displayName, role )
    local icon = KSI.window:CreateControl( "KSI" .. unit, KSI.win, CT_TEXTURE )
    icon:ClearAnchors()
    icon:SetAnchor( CENTER, KSI.win, CENTER, 0, 0 )
    icon:SetDimensions( KSI.store.iconsize, KSI.store.iconsize )
    icon:SetHidden( true )
    KSI.group[unit] = {
        ["name"]  = displayName,
        ["icon"]  = icon,
        ["role"]  = role,
        ["error"] = 0,
    }
    KSI.UpdateIcon( KSI.group[unit] )
end

 -- Creating animcache for unit
function KSI.SaveAnimIcon(displayName, texture)
	for key, val in pairs( KSI.ANIMICONS ) do
		if texture == val[1] then	
			KSI.animspecial[displayName]  = {
				["texture"]	 	= val[1],
				["width"]		= val[2],
				["height"] 		= val[3],
				["frameRate"] 	= val[4],
			}
			if KSI.store.save then
				KSI.store.animatedspecial[displayName] = KSI.animspecial[displayName]
			end
		end
	end
end

function KSI.UpdateIcon( unit )
		-- if user has anim icon, and creating of icons
	if unit.role == LFG_ROLE_ANIMATED and KSI.animshow and unit.role ~= LFG_ROLE_CUSTOMANIMATED then
		KSI.anim.RegisterUnit(unit.name)		
		KSI.anim.RegisterUnitControl(unit.name, unit.icon)	
		KSI.anim.RunUnitAnimations(unit.name)
	elseif unit.role == LFG_ROLE_CUSTOMANIMATED and KSI.animshow and unit.role ~= LFG_ROLE_ANIMATED then
		KSI.Canim.RegisterUnit(unit.name)		
		KSI.Canim.RegisterUnitControl(unit.name, unit.icon)	
		KSI.Canim.RunUnitAnimations(unit.name)
		-- if custom icon is still assigned or unit has a different role
	elseif (unit.role ~= LFG_ROLE_SPECIAL  or KSI.special[unit.name] ) and unit.role ~= LFG_ROLE_ANIMATED and unit.role ~= LFG_ROLE_CUSTOMANIMATED then
        local col  = unit.role ~= LFG_ROLE_SPECIAL and KSI.store[unit.role].color or KSI.special[unit.name].color
        local tex  = unit.role ~= LFG_ROLE_SPECIAL and KSI.store[unit.role].icon or KSI.special[unit.name].texture
        unit.icon:SetTexture( tex )
        if tex:find( "^kekus" ) ~= nil then
            unit.icon:SetColor( 1, 1, 1, KSI.store.alpha )
        else
            unit.icon:SetColor( col[1], col[2], col[3], KSI.store.alpha )
        end
    end	
end


function KSI.UpdateIcons()
    for i = 1, GROUP_SIZE_MAX do
        -- unit tag
        local tag  = "group" .. i
        local unit = KSI.group[tag]
        -- update color and texture
        if unit then
            KSI.UpdateIcon( unit )
        end
    end
end

-- clear currently assigned group icons
function KSI.ClearGroupIcons()
    if KSI.store.save then
        for key, val in pairs( KSI.special ) do
            KSI.store.cache[key] = nil
        end
    end
	KSI.Canim.UnregisterUnits()
	KSI.Canim.UnregisterUnitControls()
	KSI.Canim.StopAnimations()
    KSI.special = { }
    KSI.RestoreUserIcons()
    KSI.UpdateIcons()
    GROUP_LIST:RefreshData()
end

-- clear previously cached custom icons
function KSI.ClearIconCache()
	KSI.store.animatedspecial = { }
	KSI.animspecial = { }
	KSI.store.cache = { }
	KSI.Canim.UnregisterUnits()
	KSI.Canim.UnregisterUnitControls()
	KSI.Canim.StopAnimations()
    KSI.special = { }
    KSI.RestoreUserIcons()
    KSI.UpdateIcons()
    GROUP_LIST:RefreshData()
end

function KSI.RestoreUserIcons()
    if not KSI.store.ignore then
        for key, val in pairs( KSI.users ) do
            if not KSI.special[key] and key ~= KSI.player then
                KSI.special[key] = {
                    ["texture"] = val,
                    ["color"] = BASECOLOR
                }
            end
        end
    end
end


function KSI.ToggleUserIcons()
    if KSI.store.ignore then
        for key, val in pairs( KSI.special ) do
            if val and val.texture == KSI.users[key] then
                KSI.special[key] = nil
            end
        end
    else
        KSI.RestoreUserIcons()
    end
    KSI.UpdateIcons()
    GROUP_LIST:RefreshData()
end

-- hide all anim icons
function KSI.ToggleAnimIcons()
    KSI.animshow = not KSI.animshow
	KSI.anim.UnregisterUnits()
	KSI.anim.UnregisterUnitControls()
	KSI.anim.StopAnimations()
	KSI.Canim.UnregisterUnits()
	KSI.Canim.UnregisterUnitControls()
	KSI.Canim.StopAnimations()
	KSI.UpdateIcons()
	GROUP_LIST:RefreshData()
end


function KSI.ToggleIcons()
    KSI.show = not KSI.show
end

-- encapsulated function for ExoY
function KSI.SetIconForUnit( displayName, texture, color )
	if KSI.animshow then
	KSI.SaveAnimIcon(displayName,texture)
	end
    KSI.special[displayName] = {
        ["texture"] = texture,
        ["color"]   = color,
    }
    KSI.UpdateIcons()
    GROUP_LIST:RefreshData()
    -- store custom icon
    if KSI.store.save then
        KSI.store.cache[displayName] = KSI.special[displayName]
    end
end

-- encapsulated function for ExoY
function KSI.RemoveIconFromUnit(displayName,texture)
	KSI.store.animatedspecial[displayName] = nil
	KSI.animspecial[displayName] = nil
	KSI.Canim.UnregisterUnits(displayName )
	KSI.Canim.UnregisterUnitControls(displayName ,texture)
	KSI.Canim.StopAnimations(displayName )
    if not KSI.store.ignore and KSI.users[displayName] then
        KSI.special[displayName] = {
            ["texture"] = KSI.users[displayName],
            ["color"]   = BASECOLOR,
        }
    else
        KSI.special[displayName] = nil
    end
    KSI.UpdateIcons()
    GROUP_LIST:RefreshData()
    -- remove custom icon from store
    if KSI.store.save then
        KSI.store.cache[displayName] = nil
    end
end

function KSI.ChooseIconForUnit( displayName )
    KSI.custom.displayName = displayName
    if KSI.special[displayName] then
        KSI.custom.texture = KSI.special[displayName].texture
        KSI.custom.color   = { KSI.special[displayName].color[1], KSI.special[displayName].color[2], KSI.special[displayName].color[3] }
    else
        KSI.custom.texture = KSI.ICONS[1]
        KSI.custom.color   = BASECOLOR
    end
    KSI.picklabel:SetText( "|cffff00ICON FOR " .. string.upper( displayName ) .. "|r" )
    KSI.pickicon.icon:SetTexture( KSI.custom.texture )
    KSI.pickcolor.thumb:SetColor( KSI.custom.color[1], KSI.custom.color[2], KSI.custom.color[3], 1 )
    KSI.pickcolor.UpdateDisabled( KSI.pickcolor )
    KSI.pick:SetHidden( false )
end


function KSI.UnitErrorCheck( unit, displayName, leader )
    -- check if unit is valid
    if not DoesUnitExist( unit ) then return 1, 0 end
    if AreUnitsEqual( "player", unit ) then return 2, 0 end
    if not IsUnitPlayer( unit ) then return 3, 0 end
    if not IsUnitOnline( unit ) then return 4, 0 end
    if not IsUnitGrouped( unit ) then return 5, 0 end
    if not IsGroupMemberInSameInstanceAsPlayer( unit ) then return 6, 0 end
    if not IsGroupMemberInSameWorldAsPlayer( unit ) then return 7, 0 end
    if IsGroupMemberInRemoteRegion( unit ) then return 8, 0 end
	-- check if unit has an anim icon
	if KSI.anim.IsValidUnit(displayName) and KSI.animshow then return 0, LFG_ROLE_ANIMATED end
	-- check if unit has a custom anim icon
	if KSI.Canim.IsValidUnit(displayName) and KSI.animshow then return 0, LFG_ROLE_CUSTOMANIMATED end
    -- check if unit has a custom icon
    if KSI.special[displayName] then return 0, LFG_ROLE_SPECIAL end
    -- check if unit is group leader
    if AreUnitsEqual( leader, unit ) and KSI.store[LFG_ROLE_LEADER].show then return 0, LFG_ROLE_LEADER end
    -- check if unit has support role
    local role = GetGroupMemberSelectedRole( unit )
    if ( role ~= LFG_ROLE_HEAL or not KSI.store[LFG_ROLE_HEAL].show ) and ( role ~= LFG_ROLE_TANK or not KSI.store[LFG_ROLE_TANK].show ) and ( role ~= LFG_ROLE_DPS or not KSI.store[LFG_ROLE_DPS].show ) then return 9, 0 end
    -- unit can be rendered
    return 0, role
end

function KSI.OnUpdate()
    -- prepare render space
    Set3DRenderSpaceToCurrentCamera( KSI.ctrl:GetName() )
    -- retrieve camera world position and orientation vectors
    local cX, cY, cZ = GuiRender3DPositionToWorldPosition( KSI.ctrl:Get3DRenderSpaceOrigin() )
    local fX, fY, fZ = KSI.ctrl:Get3DRenderSpaceForward()
    local rX, rY, rZ = KSI.ctrl:Get3DRenderSpaceRight()
    local uX, uY, uZ = KSI.ctrl:Get3DRenderSpaceUp()
    -- https://semath.info/src/inverse-cofactor-ex4.html
    -- calculate determinant for camera matrix
    -- local det = rX * uY * fZ - rX * uZ * fY - rY * uX * fZ + rZ * uX * fY + rY * uZ * fX - rZ * uY * fX
    -- local mul = 1 / det
    -- determinant should always be -1
    -- instead of multiplying simply negate
    -- calculate inverse camera matrix
    local i11 = -( uY * fZ - uZ * fY )
    local i12 = -( rZ * fY - rY * fZ )
    local i13 = -( rY * uZ - rZ * uY )
    local i21 = -( uZ * fX - uX * fZ )
    local i22 = -( rX * fZ - rZ * fX )
    local i23 = -( rZ * uX - rX * uZ )
    local i31 = -( uX * fY - uY * fX )
    local i32 = -( rY * fX - rX * fY )
    local i33 = -( rX * uY - rY * uX )
    local i41 = -( uZ * fY * cX + uY * fX * cZ + uX * fZ * cY - uX * fY * cZ - uY * fZ * cX - uZ * fX * cY )
    local i42 = -( rX * fY * cZ + rY * fZ * cX + rZ * fX * cY - rZ * fY * cX - rY * fX * cZ - rX * fZ * cY )
    local i43 = -( rZ * uY * cX + rY * uX * cZ + rX * uZ * cY - rX * uY * cZ - rY * uZ * cX - rZ * uX * cY )
    -- screen dimensions
    local uiW, uiH = GuiRoot:GetDimensions()
    -- group leader
    local leader = GetGroupLeaderUnitTag()
    -- drawing order
    local ztotal = 0
    local zorder = {}
    -- update group icons
    for i = 1, GROUP_SIZE_MAX do
        -- unit tag
        local unit = "group" .. i
        -- hide unit icon
        if KSI.group[unit] then
            KSI.group[unit].icon:SetHidden( true )
        end
        -- retrieve unit display name
        local displayName = string.lower( GetUnitDisplayName( unit ) )
        -- check for errors
        local error, role = KSI.UnitErrorCheck( unit, displayName, leader )
        if error == 0 and KSI.show then
            -- check if the unit needs an icon
            if not KSI.group[unit] then
                KSI.CreateSupportIcon( unit, displayName, role )
            end
            -- clear previous errors
            KSI.group[unit].error = error
            -- unit world position
            local _, wX, wY, wZ = GetUnitRawWorldPosition( unit )
            wY = wY + KSI.store.offset * 100
            -- calculate unit view position
            local pX = wX * i11 + wY * i21 + wZ * i31 + i41
            local pY = wX * i12 + wY * i22 + wZ * i32 + i42
            local pZ = wX * i13 + wY * i23 + wZ * i33 + i43
            -- if unit is in front
            if pZ > 0 then
                -- calculate unit screen position
                local w, h = GetWorldDimensionsOfViewFrustumAtDepth( pZ )
                local x, y = pX * uiW / w, -pY * uiH / h
                -- update icon position
                local icon = KSI.group[unit].icon
                icon:ClearAnchors()
                icon:SetAnchor( CENTER, KSI.win, CENTER, x, y )
                -- update icon texture and color based on role
                if role ~= KSI.group[unit].role or displayName ~= KSI.group[unit].name then
                    KSI.group[unit].role = role
                    KSI.group[unit].name = displayName
                    KSI.UpdateIcon( KSI.group[unit] )
                end
                -- update icon size
                if KSI.store.scaling then
                    local scaled = KSI.store.iconsize * 1000 / pZ -- dist / pZ
                    icon:SetDimensions( scaled, scaled )
                else
                    icon:SetDimensions( KSI.store.iconsize, KSI.store.iconsize )
                end
                -- show icon
                icon:SetHidden( false )
                -- handle draw order
                zorder[1 + zo_floor( pZ )] = icon
                ztotal = ztotal + 1
            end
        else
            -- debug errors in chat
            if KSI.debug and KSI.group[unit] and KSI.group[unit].error ~= error then
                d( "|cffffff[KSI]|r |cff0000" .. error .. "|r |c00ff00" .. unit .. "|r (" .. displayName .. ") " .. ERRORS[error] )
                KSI.group[unit].error = error
            end
        end
    end
    if ztotal > 0 then
        local keys = { }
        for k in pairs( zorder ) do
            table.insert( keys, k )
        end
        table.sort( keys )
        -- adjust draw order
        for _, k in ipairs( keys ) do
            zorder[k]:SetDrawLevel( ztotal )
            ztotal = ztotal - 1
        end
    end
end

function KSI.CreateOptions()
	local panelData = {
		type				= "panel",
		name				= KSI.name,
		displayName			= KSI.name,
		author				= KSI.author,
		version				= KSI.version,
		slashCommand		= "/KSI",
		registerForRefresh	= true,
		registerForDefaults = true,
    }

	local optionsTable = {
		{
			type = "description",
            text = "This addon shows icons for players in your group and works in dungeons and trials as well as when hiding group members with the Crown Crate trick.",
		},
		{
			type = "description",
            text = "Use the |cffff00/KSI|r command to open the addon settings, color can only be changed for icons provided by the game, settings are saved account-wide. Don't forget to assign a |c00ffffkeybinding|r in your controls settings to quickly toggle icon visibility.",
		},
        {
			type  = "description",
			text  = "",
			title = "\n|cffff00GENERAL SETTINGS|r",
        },
        {
            type    = "slider",
            name    = "Update Interval",
            min     = 0,
            max     = 100,
            step    = 10,
            default = DEFAULT.interval,
            getFunc = function() return KSI.store.interval end,
            setFunc = function( newValue ) KSI.store.interval = newValue; KSI.StartPolling() end,
        },
        {
            type    = "slider",
            name    = "Icon Size",
            min     = 25,
            max     = 250,
            step    = 5,
            default = DEFAULT.iconsize,
            getFunc = function() return KSI.store.iconsize end,
            setFunc = function( newValue ) KSI.store.iconsize = newValue end,
        },
        {
            type     = "slider",
            name     = "Vertical Offset",
            min      = 0.5,
            max      = 5.0,
            step     = 0.1,
            decimals = 2,
            default  = DEFAULT.offset,
            getFunc  = function() return KSI.store.offset end,
            setFunc  = function( newValue ) KSI.store.offset = newValue end,
        },
        -- {
        --     type     = "slider",
        --     name     = "Scaling Intensity",
        --     min      = 0,
        --     max      = 1,
        --     step     = 0.05,
        --     decimals = 2,
        --     default  = DEFAULT.scaling,
        --     getFunc  = function() return KSI.store.scalefactor end,
        --     setFunc  = function( newValue ) KSI.store.scalefactor = newValue end,
        -- },
        {
            type     = "slider",
            name     = "Opacity",
            min      = 0.1,
            max      = 1,
            step     = 0.05,
            decimals = 2,
            default  = DEFAULT.alpha,
            getFunc  = function() return KSI.store.alpha end,
            setFunc  = function( newValue ) KSI.store.alpha = newValue; KSI.UpdateIcons() end,
        },
        {
            type     = "checkbox",
            name     = "Scaling",
            default  = DEFAULT.scaling,
            getFunc  = function() return KSI.store.scaling end,
            setFunc  = function( newValue ) KSI.store.scaling = newValue end,
        },
    }

    local function RoleOptions( role, description )
        table.insert( optionsTable, {
            type  = "description",
            text  = "",
            title = "\n|cffff00" .. string.upper( description ) .. " ICON|r",
        } )
        table.insert( optionsTable, {
            type    = "checkbox",
            name    = "Show " .. description .. " Icon",
            default = DEFAULT[role].show,
            getFunc = function() return KSI.store[role].show end,
            setFunc = function( newValue ) KSI.store[role].show = newValue end,
        } )
        table.insert( optionsTable, {
            type     = "colorpicker",
            name     = "Icon Color",
            default  = { r = DEFAULT[role].color[1], g = DEFAULT[role].color[2], b = DEFAULT[role].color[3] },
            getFunc  = function() return unpack( KSI.store[role].color ) end,
            setFunc  = function( r, g, b, a ) KSI.store[role].color = { r, g, b }; KSI.UpdateIcons() end,
            disabled = function() return not KSI.store[role].show or KSI.store[role].icon:find( "^kekus" ) ~= nil end,
        } )
        table.insert( optionsTable, {
            type       = "iconpicker",
            name       = "Texture",
            iconSize   = 64,
            maxColumns = 4,
            default    = DEFAULT[role].icon,
            getFunc    = function() return KSI.store[role].icon end,
            setFunc    = function( newValue ) KSI.store[role].icon = newValue; KSI.UpdateIcons() end,
            disabled   = function() return not KSI.store[role].show end,
            choices    = KSI.ICONS,
        } )
    end

    RoleOptions( LFG_ROLE_LEADER, "Leader" )
    RoleOptions( LFG_ROLE_TANK, "Tank" )
    RoleOptions( LFG_ROLE_HEAL, "Healer" )
    RoleOptions( LFG_ROLE_DPS, "DPS" )

    local furtherOptions = {
        {
            type  = "description",
            text  = "",
            title = "\n|cffff00CUSTOM ICONS|r",
        },
        {
            type  = "description",
            text  = "To assign custom icons to individual players, right-click on that person in the group window and select |c00ffffAdd Icon|r. Once assigned, custom icons will appear next to the selected player in the group window. Right-click again to |c00ffffChange Icon|r or |c00ffffRemove Icon|r.",
        },
        {
            type     = "checkbox",
            name     = "Remember assigned Icons",
            default  = DEFAULT.save,
            getFunc  = function() return KSI.store.save end,
            setFunc  = function( newValue ) KSI.store.save = newValue end,
        },
        {
            type    = "button",
            name    = "Clear Group",
            width   = "half",
            func    = function() KSI.ClearGroupIcons() end,
            warning = "This will remove all assigned icons for your current group members.",
        },
        {
            type    = "button",
            name    = "Empty Cache",
            width   = "half",
            func    = function() KSI.ClearIconCache() end,
            warning = "This will remove all previously cached icons for individual players.",
        },
        {
			type  = "description",
			text  = "",
			title = "\n|cffff00UNIQUE ICONS|r",
        },
        {
			type = "description",
            text = "Unique icons are automatically assigned to the respective player and can not be removed. If you don't want to see them, just turn on the option to ignore them.",
		},
        {
            type     = "checkbox",
            name     = "Ignore unique Icons",
            default  = DEFAULT.ignore,
            getFunc  = function() return KSI.store.ignore end,
            setFunc  = function( newValue ) KSI.store.ignore = newValue; KSI.ToggleUserIcons() end,
        },
		{
			type  = "description",
			text  = "",
			title = "\n|cffff00YOUR ICON|r",
        },
		{
			type = "description",
            text = "Your unique icon, if you have one",
		},
		{
            type     = "checkbox",
            name     = "Ignore your icon",
            default  = DEFAULT.youricon,
            getFunc  = function() return KSI.store.youricon end,
            setFunc  = function( newValue ) KSI.store.youricon = newValue; end,
			warning = "This will remove your icon from group list.",
			requiresReload = true,
        },
		{
			type  = "description",
			text  = "",
			title = "\n|cffff00ANIMATED ICONS|r",
        },
		{
			type = "description",
            text = "Unique animated icons are automatically assigned to the respective player and can not be removed. If you don't want to see them turn on the option to ignore them.",
		},
		{
            type     = "checkbox",
            name     = "Ignore animated icons",
            default  = DEFAULT.animicons,
            getFunc  = function() return KSI.store.animicons end,
            setFunc  = function( newValue ) KSI.store.animicons = newValue; KSI.ToggleAnimIcons() end,
			warning = "This will remove all animated icons from users.",
			requiresReload = true,
        },
        {
            type = "description",
            text = "",
        },
    }

    tableAppend( optionsTable, furtherOptions )

	LAM:RegisterAddonPanel( KSI.name .. "Options", panelData )
	LAM:RegisterOptionControls( KSI.name .. "Options", optionsTable )
end

function KSI.CreateMenu()
    -- create add/change/remove icon context menu for group list
    local onMouseUp = GROUP_LIST.GroupListRow_OnMouseUp
    function GROUP_LIST:GroupListRow_OnMouseUp( control, button, upInside )
        onMouseUp( self, control, button, upInside )
        local unit = ZO_ScrollList_GetData( control )
        if button == MOUSE_BUTTON_INDEX_RIGHT and upInside then
            local name = string.lower( unit.displayName )
			-- check if user has anim icon or anim icons off
			if not KSI.anim.IsValidUnit(name) or KSI.store.animicons then
            if name ~= KSI.player then
				if KSI.special[name] then
					AddMenuItem( "Change Icon", function() KSI.ChooseIconForUnit( name ) end )
					if KSI.special[name].texture ~= KSI.users[name] then
						AddMenuItem( "Remove Icon", function() KSI.RemoveIconFromUnit( name ) end )
					end
				else
					AddMenuItem( "Add Icon", function() KSI.ChooseIconForUnit( name ) end )
				end
				self:ShowMenu( control )
				end  
			end
        end
    end	

    -- show custom icons in group list
	ZO_PostHook( GROUP_LIST, "RefreshData", function()
		if not IsUnitGrouped( "player" ) then return end
        local ctrl = ZO_GroupList:GetNamedChild( "List" ):GetNamedChild( "Contents" )
        for i = 1, ctrl:GetNumChildren() do
            local child = ctrl:GetChild( i )
			local icon  = child:GetNamedChild( "Leader" ):GetNamedChild( "Icon" )
			local unit  = ZO_ScrollList_GetData( child ) or ZO_ScrollList_GetData( ctrl )
            if unit and icon then
                local displayName = string.lower( unit.displayName )
				-- animated icons in group, you should use callLater function 
				if KSI.anim.IsValidUnit(displayName) then
				zo_callLater(function()
					if KSI.animshow and not KSI.store.animicons then 						
						KSI.anim.RegisterUnitControl(displayName, icon)	
						KSI.anim.RunUnitAnimations(displayName)
					else
						KSI.anim.UnregisterUnitControls(displayName, icon)
						KSI.anim.StopAnimations(displayName)
					end 
				end, 0.001)
				elseif KSI.Canim.IsValidUnit(displayName) then
				zo_callLater(function()
					if KSI.animshow and not KSI.store.animicons then 						
						KSI.Canim.RegisterUnitControl(displayName, icon)	
						KSI.Canim.RunUnitAnimations(displayName)
					else
						KSI.Canim.UnregisterUnitControls(displayName, icon)
						KSI.Canim.StopAnimations(displayName)
					end 
				end, 0.001)
				end		
				if KSI.special[displayName] and displayName ~= KSI.player then 
					local col = KSI.special[displayName].color 					
					local tex = KSI.special[displayName].texture 
					icon:SetTexture( tex )
					if tex:find( "^kekus" ) ~= nil then
						icon:SetColor( 1, 1, 1, 1 )
					else
						icon:SetColor( col[1], col[2], col[3], 1 )
					end
					icon:SetHidden( false )				
					elseif unit.leader then
					icon:SetTexture( "esoui/art/lfg/lfg_leader_icon.dds" )
					icon:SetColor( 1, 1, 1, 1 )
					icon:SetHidden( false )
					else
					-- empty texture
					icon:SetTexture("KekusSupportIcons/icons/misc/empty.dds")
					icon:SetHidden( false )
				end		
				if displayName == KSI.player and KSI.youricon then
					if KSI.anim.IsValidUnit(displayName) then
						if KSI.animshow and not KSI.store.animicons then 		
							KSI.anim.RegisterUnit(displayName)
							KSI.anim.RegisterUnitControl(displayName, icon)	
							KSI.anim.RunUnitAnimations(displayName)
						else
							KSI.anim.UnregisterUnit(displayName)
							KSI.anim.UnregisterUnitControl(displayName, icon)
							KSI.anim.StopUnitAnimations(displayName)
						end
					end
					local atex = KSI.users[displayName]
					icon:SetTexture(atex)
					icon:SetHidden( false )
				end				
			end		
		end
	end)
end

	
function KSI.StartPolling()
    KSI.events:UnregisterForUpdate( KSI.name .. "Update" )
    KSI.events:RegisterForUpdate( KSI.name .. "Update", KSI.store.interval, KSI.OnUpdate )
end

function KSI.OnActivated()
    KSI.events:UnregisterForEvent( KSI.name .. "Activated", EVENT_PLAYER_ACTIVATED )
    KSI.CreateMenu()
    KSI.StartPolling()
end

function KSI.OnAddonLoaded( event, addonName )
    if addonName ~= KSI.name then return end
    ZO_CreateStringId( "SI_BINDING_NAME_KSI_TOGGLE", "Toggle Icon visibility" )
    KSI.store = ZO_SavedVars:NewAccountWide( "KSIStore", 1, nil, DEFAULT )
	if KSI.store.animicons then
		KSI.animshow = not KSI.animshow
	end
	if KSI.store.youricon then
		KSI.youricon = not KSI.youricon
	end
	-- iconpicker data
	KSI.custom      = {
		displayName = nil,
		color       = BASECOLOR,
		texture     = KSI.ICONS[1]
	}
    KSI.CreateUI()
    KSI.CreateUICustom()
    KSI.CreateOptions()
    KSI.RestoreUserIcons()
    -- prepare cached custom icons
    if KSI.store.save then
        for key, val in pairs( KSI.store.cache ) do
            if val then
                KSI.special[key] = val
            end
        end
    end
    GROUP_LIST:RefreshData()
    KSI.events:UnregisterForEvent( KSI.name, EVENT_ADD_ON_LOADED )
	KSI.events:RegisterForEvent( KSI.name .. "Activated", EVENT_PLAYER_ACTIVATED, KSI.OnActivated )
end

KSI.events:RegisterForEvent( KSI.name, EVENT_ADD_ON_LOADED, KSI.OnAddonLoaded )
