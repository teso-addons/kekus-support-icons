KekusSupportIcons.Canim = {
	name = "KekusSupportIcons_Canim",
	users = {},
}

local KSI = KekusSupportIcons
local M = KSI.Canim
local anims = {}

-- Format: {"icon_path", columns, rows, frame_rate}

-- Initialize a unit.
function M.RegisterUnit(unit)
	if KSI.store.animatedspecial[unit] then
		if anims[unit] then M.UnregisterUnit(unit) end
		anims[unit] = {
			timeline = ANIMATION_MANAGER:CreateTimeline(),
			width = KSI.store.animatedspecial[unit].width,
			height = KSI.store.animatedspecial[unit].height,
			frameRate = KSI.store.animatedspecial[unit].frameRate,
		}
		return true
	end
	return false
end

-- Delete all unit animations.
function M.UnregisterUnit(unit)
	if KSI.store.animatedspecial[unit] then
		M.StopUnitAnimations(unit)
		anims[unit] = nil
	end
end

-- Clear all unit.
function M.UnregisterUnits()
	M.StopAnimations()
	anims = {}
end

-- Returns true if a unit is in KSI.anim.unit table.
function M.IsValidUnit(unit)
	return KSI.store.animatedspecial[unit] ~= nil
end

-- Register a texture control to update.
function M.RegisterUnitControl(unit, control)
	local a = anims[unit]
	if a then
		local animation = a.timeline:InsertAnimation(ANIMATION_TEXTURE, control)
		animation:SetImageData(a.width, a.height)
		animation:SetFramerate(a.frameRate)
		control:SetTexture(KSI.store.animatedspecial[unit].texture)
	end
end

-- Unregister all controls.
function M.UnregisterUnitControls(unit)
	local a = anims[unit]
	if a then
		a.timeline:SetEnabled(false)
		a.timeline = ANIMATION_MANAGER:CreateTimeline()
	end
end

-- Start all animations for a unit.
function M.RunUnitAnimations(unit)
	local a = anims[unit]
	if a then
		a.timeline:SetEnabled(true)
		a.timeline:SetPlaybackType(ANIMATION_PLAYBACK_LOOP, LOOP_INDEFINITELY)
		a.timeline:PlayFromStart()
	end
end

-- Stop all animations for a unit.
function M.StopUnitAnimations(unit)
	local a = anims[unit]
	if a then
		a.timeline:SetEnabled(false)
	end
end

-- Play all animations.
function M.RunAnimations()
	for name, _ in pairs(anims) do
		M.RunUnitAnimations(name)
	end
end

-- Stop all animations.
function M.StopAnimations()
	for name, _ in pairs(anims) do
		M.StopUnitAnimations(name)
	end
end
