local KSI = KekusSupportIcons

 
KSI.ICONS = {
	-- standart icons
    "esoui/art/icons/mapkey/mapkey_groupleader.dds",
    "esoui/art/icons/guildranks/guild_rankicon_leader_large.dds",
    "esoui/art/lfg/gamepad/lfg_roleicon_tank.dds",
    "esoui/art/lfg/gamepad/lfg_roleicon_healer.dds",
    "esoui/art/lfg/gamepad/lfg_roleicon_dps.dds",
    "esoui/art/tutorial/gamepad/gp_lfg_normaldungeon.dds",
    "esoui/art/tutorial/gamepad/gp_lfg_veteranldungeon.dds",
    "esoui/art/icons/mapkey/mapkey_groupboss.dds",
    "kekussupporticons/icons/trollface.dds",
    "kekussupporticons/icons/donut.dds",
    "kekussupporticons/icons/emoji-crazy.dds",
    "kekussupporticons/icons/lightning-bolt.dds",
    "kekussupporticons/icons/emoji-poop.dds",
    "kekussupporticons/icons/wolf.dds",
    "kekussupporticons/icons/pepe.dds",
    "kekussupporticons/icons/green-arrow.dds",
    "kekussupporticons/icons/hellboy.dds",
    "kekussupporticons/icons/fawkes.dds",
    "kekussupporticons/icons/pig.dds",
    "kekussupporticons/icons/stuart.dds",
	"kekussupporticons/icons/ugabuga.dds",
	"kekussupporticons/icons/UgaKing.dds",
	"kekussupporticons/icons/UgaTank.dds",
	"kekussupporticons/icons/UgaHealer.dds",
	"kekussupporticons/icons/Valacas.dds",
	"kekussupporticons/icons/DyaDyaSasha.dds",
	"kekussupporticons/icons/Kalivan.dds",
	"kekussupporticons/icons/Tugarin-Zmei.dds",
	"kekussupporticons/icons/VarSus.dds",
	"kekussupporticons/icons/Jew.dds",
	"kekussupporticons/icons/Narmalna.dds",
	"kekussupporticons/icons/Shashlik.dds",
	"kekussupporticons/icons/zloi.dds",
	"kekussupporticons/icons/Cool-Cat.dds",
	"kekussupporticons/icons/Zloi-Chel.dds",
	"kekussupporticons/icons/Pinya.dds",
	"kekussupporticons/icons/Smolet.dds",
	"kekussupporticons/icons/Livsi.dds",	
	"kekussupporticons/icons/SadKit.dds",
	"kekussupporticons/icons/Vanychka.dds",
	"kekussupporticons/icons/Babka1.dds",
	"kekussupporticons/icons/Babka2.dds",
	"kekussupporticons/icons/Babka3.dds",
	"kekussupporticons/icons/Buryat1.dds",
	"kekussupporticons/icons/Buryat2.dds",	
	"kekussupporticons/icons/misc/BIG-RICARDO.dds",
	"kekussupporticons/icons/misc/Ricardo.dds",
}

-- Format: {"icon_path", columns, rows, frame_rate}

KSI.ANIMICONS = {
	-- custom anim icons
	{"kekussupporticons/icons/misc/Ricardo.dds", 4, 4, 10},
	{"kekussupporticons/icons/misc/BIG-RICARDO.dds", 8, 8, 10},
	{"kekussupporticons/icons/misc/Jaba.dds", 5, 2, 10},
	{"kekussupporticons/icons/misc/Zloi_Ded.dds", 4, 3, 12},
}


KSI.users = {
    -- misc
    ["@forsion"]            = "kekussupporticons/icons/misc/Forsion.dds",
    ["@rouderyt"]           = "kekussupporticons/icons/misc/Rouderyt.dds",
    ["@kekusthedivine"]     = "kekussupporticons/icons/misc/Kekus.dds",
	["@meinherz"]           = "kekussupporticons/icons/misc/meinherz.dds",
	["@pururumich"]         = "kekussupporticons/icons/misc/Pururum.dds",
	["@thekiller95sd"]      = "kekussupporticons/icons/misc/keker.dds",
}

-- Format: {"icon_path", columns, rows, frame_rate}

KSI.anim.users = {
	--anim   	
	["@kekusthedivine"] 	= {"kekussupporticons/icons/misc/KekusGIF.dds", 8, 8, 20},
	["@forsion"]            = {"kekussupporticons/icons/misc/Steve_Rambo.dds", 11, 2, 11},
	-- {"kekussupporticons/icons/misc/BIG-RICARDO.dds", 8, 8, 10},
	-- {"kekussupporticons/icons/misc/Zloi_Ded.dds", 4, 3, 12},
	-- {"kekussupporticons/icons/misc/Jaba.dds", 5, 2, 10},
	-- {"kekussupporticons/icons/misc/Ricardo.dds", 4, 4, 10},
}